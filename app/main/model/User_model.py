from app import db


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100), nullable=False, unique=True)
    password = db.Column(db.String(100))
    public_id = db.Column(db.Integer)

    def __repr__(self):
        return f'<User {self.id}>'

    def json(self):
        return {"id": self.id, "name": self.name, "password": self.password}
