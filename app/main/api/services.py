from flask import Blueprint, request, redirect, url_for

from app.main.model.Article_model import Article
from app.main.api.auth import token_required
from app import db
from app import cache

bp = Blueprint('service', __name__)


@bp.route('/')
# @cache.cached(timeout=10000)
def index():
    articles = Article.query.order_by(Article.id.desc()).limit(5).all()
    return {"Articles": list(x.json() for x in articles)}


@bp.route('/page')
# @cache.cached(timeout=10000)
def index_paging():
    data = request.get_json()
    articles = Article.query.paginate(page=int(data['page']), per_page=int(data['size']))
    return {"Articles": list(x.json() for x in articles.items)}


@bp.route('/<int:id_article>')
# @cache.cached(timeout=10000)
def index_article(id_article):
    article = Article.query.filter_by(id=id_article).first()
    if article is None:
        return "ID not found!!"
    return article.json()


@bp.route('/create', methods=['POST', 'GET'])
@token_required
def create(current_user):
    if request.method == 'POST':
        data = request.get_json()

        article = Article(title=data['title'], context=data['context'])

        db.session.add(article)
        db.session.commit()

        return article.json()
    return redirect(url_for('index'))


@bp.route('/update/<int:article_id>', methods=['PUT', 'GET'])
@token_required
def update(current_user, article_id):
    data = request.get_json()

    article = Article.query.get_or_404(article_id)

    if article is None:
        return {"message": "ID not found"}

    if request.method == 'PUT':

        if data['title'] is not None:
            article.title = data['title']

        if data['context'] is not None:
            article.context = data['context']

        db.session.add(article)
        db.session.commit()

        return {"message": "Update Success"}
    return redirect(url_for('index'))


@bp.route('/delete/<int:article_id>')
@token_required
def delete(current_user, article_id):
    article = Article.query.get_or_404(article_id)
    db.session.delete(article)
    db.session.commit()
    return redirect(url_for('index'))


@bp.route('/search', methods=['POST', ])
# @cache.cached(timeout=10000)
def search():
    if request.method == 'POST':
        data = request.get_json()
        article = Article.query.filter_by(title=data['title']).first()
        return article.json()
