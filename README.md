# Mini_Project_5_Deploy

## Cách Sử Dụng
(yêu cầu có sẵn [Docker](https://www.docker.com/), [docker-compose](https://docs.docker.com/compose/) trong máy)

- Clone project
```bash
git clone https://gitlab.com/ducminhtong2k3/tf_bai5_deploy.git
```
- Mở terminal, cd vào folder
```bash
cd tf_bai5_deploy
```

- Chạy project trên docker-compose
```bash
docker-compose up --build
```

- Truy cập địa chỉ [localhost](http://localhost/)
